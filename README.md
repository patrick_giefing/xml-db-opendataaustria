# OpenDataAustria ExistDB use case

## Documentation

[LaTeX-document (german)](./src/main/document/src/main.pdf)

## Architecture

The use of the Postgres in the use case doesn't make much sense but it was necessary for the assignment.

Backend: Kotlin, Gradle, SpringBoot, SpringData

![architecture](./src/main/document/src/screenshots/XmlDB-UseCase-Architecture.png)

## GUI

### React

When chaning the input field for the metro station opening year ("Eröffnungsjahr"), an XQuery request is sent do ExistDB. 

![overview](./src/main/document/src/screenshots/gui-overview.png)

### Jupyter Notebook

![overview](./src/main/document/src/screenshots/jupyter-notebook-output.png)

![overview](./src/main/document/src/screenshots/validate-xml-dialog.png)

## XQueries in ExistDB GUI

![overview](./src/main/document/src/screenshots/metro-eroeffnung-jahr-existdb-gui.png)

![overview](./src/main/document/src/screenshots/validate-xml-exist-gui.png)