package at.ac.fhwn.dbsystems.xmldb.existdb

import org.exist.xmldb.DatabaseImpl
import org.exist.xmldb.EXistResource
import org.xmldb.api.DatabaseManager
import org.xmldb.api.base.*
import org.xmldb.api.base.Collection
import org.xmldb.api.modules.CollectionManagementService
import org.xmldb.api.modules.XMLResource
import org.xmldb.api.modules.XQueryService

/**
 * http://exist-db.org/exist/apps/doc/devguide_xmldb
 */
class ExistDbClient(val uri: String, val context: String, val user: String, val password: String) {
    fun registerExistDB() {
        val dbs = DatabaseManager.getDatabases()
        val dbRegisteredExist = dbs.find { it.names.contains("exist") }
        if (dbRegisteredExist === null) {
            val db = DatabaseImpl()
            db.setProperty("create-database", "true")
            DatabaseManager.registerDatabase(db)
        }
    }

    @Throws(XMLDBException::class)
    fun getOrCreateCollection(collectionUri: String): Collection {
        return getOrCreateCollection(context + collectionUri, 0)
    }

    @Throws(XMLDBException::class)
    private fun getOrCreateCollection(collectionUri: String, pathSegmentOffset: Int): Collection {
        var collectionUri = collectionUri
        var pathSegmentOffset = pathSegmentOffset
        var col = getCollection(collectionUri)
        if (col !== null)
            return col
        if (collectionUri.startsWith("/")) {
            collectionUri = collectionUri.substring(1)
        }
        val pathSegments = collectionUri.split("/").toTypedArray()
        if (pathSegments.isNotEmpty()) {
            val path = StringBuilder()
            for (i in 0..pathSegmentOffset) {
                path.append("/" + pathSegments[i])
            }
            val start = getCollection(path.toString())
            if (start == null) {
                val parentPath = path.substring(0, path.lastIndexOf("/"))
                val parent = getCollection(parentPath)
                val mgt = parent?.getService("CollectionManagementService", "1.0") as CollectionManagementService
                col = mgt.createCollection(pathSegments[pathSegmentOffset])
                col.close()
                parent?.close()
            } else {
                start.close()
            }
        }
        return getOrCreateCollection(collectionUri, ++pathSegmentOffset)
    }

    private fun getCollection(path: String): Collection? {
        return DatabaseManager.getCollection(uri + path, user, password)
    }

    fun execQuery(collectionUri: String, query: String): List<String> {
        val col = getOrCreateCollection(collectionUri)
        val xqs = col.getService("XQueryService", "3.1") as XQueryService

        val compiled: CompiledExpression =
            xqs.compile(query)
        val result = xqs.execute(compiled)
        val i: ResourceIterator = result.getIterator()
        var res: Resource? = null
        val results = mutableListOf<String>()
        while (i.hasMoreResources()) {
            try {
                res = i.nextResource()
                results.add(res.getContent().toString())
            } finally {
                try {
                    (res as EXistResource?)!!.freeResources()
                } catch (xe: XMLDBException) {
                    xe.printStackTrace()
                }
            }
        }
        return results
    }

    fun saveXmlRessource(col: Collection, xmlFileName: String, xml: Any) {
        val xmlResource = col.createResource(xmlFileName, "XMLResource") as XMLResource
        xmlResource.content = xml
        col.storeResource(xmlResource)
    }
}