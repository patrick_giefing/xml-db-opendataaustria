package at.ac.fhwn.dbsystems.xmldb.controller

import at.ac.fhwn.dbsystems.xmldb.XmlProps
import at.ac.fhwn.dbsystems.xmldb.existdb.ExistDbClient
import at.ac.fhwn.dbsystems.xmldb.model.json.JsonRecord
import at.ac.fhwn.dbsystems.xmldb.repo.JsonRecordRepo
import com.fasterxml.jackson.databind.ObjectMapper
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.net.URL
import java.util.*

@Controller
@RestController
@RequestMapping("oda")
open class OpenDataAustriaController @Autowired constructor(
    private val existDbClient: ExistDbClient,
    private val jsonRecordRepo: JsonRecordRepo,
    private val objectMapper: ObjectMapper
) {
    @GetMapping("metro/fetch/")
    open fun fetchMetroData() {
        val xsdMetro =
            OpenDataAustriaController::class.java.classLoader.getResourceAsStream("metro-stations.xsd").readAllBytes()
        val xmlMetro = readRecordsFromUrlAndSaveForGroupAndReturnXml(
            "https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:UBAHNHALTOGD&srsName=EPSG:4326&outputFormat=json",
            XmlProps.groupMetro
        )
        val colMetro = existDbClient.getOrCreateCollection(XmlProps.collectionMetro)
        existDbClient.saveXmlRessource(colMetro, XmlProps.xmlFileNameMetro, xmlMetro)
        existDbClient.saveXmlRessource(colMetro, XmlProps.xsdFileNameMetro, xsdMetro)
    }

    @Transactional
    open fun readRecordsFromUrlAndSaveForGroupAndReturnXml(url: String, group: String): String {
        val records = getRecords(url)
        val jsonRecords = convertJsonObjectsToRecords(records, group)
        jsonRecordRepo.deleteByRecordGroup(group)
        jsonRecordRepo.saveAll(jsonRecords)
        val xml = jsonRecordRepo.fetchXmlAggregation(group)
        return xml
    }

    private fun convertJsonObjectsToRecords(records: List<*>, group: String): List<JsonRecord> {
        val jsonRecords = records.map { record ->
            val jsonRecord = JsonRecord()
            jsonRecord.let {
                it.recordGroup = group
                it.recordId = UUID.randomUUID().toString()
                it.json = objectMapper.writeValueAsString(record)
            }
            return@map jsonRecord
        }
        return jsonRecords
    }

    @GetMapping("metro/stations/opened/{year}/")
    open fun stationsOpenedInYear(@PathVariable("year") year: Int): List<*> {
        val xmlResults: List<String> = existDbClient.execQuery(
            XmlProps.collectionMetro,
            """doc("${XmlProps.xmlFileNameMetro}")//station[./properties/eroeffnung_jahr=$year]"""
        )
        return convertXmlsToJsonObjects(xmlResults)
    }

    @GetMapping("metro/validate/", produces = [MediaType.APPLICATION_XML_VALUE])
    open fun validateMetroXml(): String {
        val xmlResults = existDbClient.execQuery(
            XmlProps.collectionMetro,
            """validation:jing-report(doc("${XmlProps.xmlFileNameMetro}"),doc("${XmlProps.xsdFileNameMetro}"))"""
        )
        return xmlResults[0]
    }

    fun getRecords(url: String): List<*> {
        val bytes = URL(url).readBytes()
        val map = objectMapper.readValue(bytes, Map::class.java)
        return map["features"] as List<*>
    }

    private fun convertXmlsToJsonObjects(xmls: List<String>): List<Map<*, *>> {
        return xmls.map { xml -> XML.toJSONObject(xml).toMap() }
    }

    @RequestMapping("metro/xml/", produces = [MediaType.APPLICATION_XML_VALUE])
    open fun getMetroXml(): String? {
        val col = existDbClient.getOrCreateCollection(XmlProps.collectionMetro)
        return col.getResource(XmlProps.xmlFileNameMetro)?.content?.toString()
    }

    @RequestMapping("metro/xsd/", produces = [MediaType.APPLICATION_XML_VALUE])
    open fun getMetroXsd(): String? {
        val col = existDbClient.getOrCreateCollection(XmlProps.collectionMetro)
        return col.getResource(XmlProps.xsdFileNameMetro)?.content?.toString()
    }
}