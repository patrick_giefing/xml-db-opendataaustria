package at.ac.fhwn.dbsystems.xmldb.repo

import at.ac.fhwn.dbsystems.xmldb.model.json.JsonRecord
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface JsonRecordRepo : JpaRepository<JsonRecord, String> {
    @Query(
        value = """
select
  xmlelement(name stations, (
    select xmlagg(
      xmlelement(NAME station,
        xmlelement(NAME geometry_name, json_object ->> 'geometry_name'),
        xmlelement(NAME coordinates,
          xmlelement(NAME long, json_object -> 'geometry' -> 'coordinates' -> 0),
          xmlelement(NAME lat,  json_object -> 'geometry' -> 'coordinates' -> 1)
        ),
        xmlelement(name properties, (select xmlagg(('<' || lower(key) || '>' || value || '</' || lower(key) || '>')\:\:xml) from json_each_text(json_object -> 'properties')))
      )
    )from json_record where record_group = :recordGroup
  )
)\:\:text as xml
    """, nativeQuery = true
    )
    fun fetchXmlAggregation(@Param("recordGroup") recordGroup: String): String;

    @Transactional
    @Modifying
    fun deleteByRecordGroup(recordGroup: String);
}