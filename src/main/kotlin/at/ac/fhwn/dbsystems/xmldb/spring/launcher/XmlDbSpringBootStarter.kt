package at.ac.fhwn.dbsystems.xmldb.spring.launcher

import at.ac.fhwn.dbsystems.xmldb.ProjectPackageClassPath
import at.ac.fhwn.dbsystems.xmldb.controller.OpenDataAustriaController
import at.ac.fhwn.dbsystems.xmldb.existdb.ExistDbClient
import at.ac.fhwn.dbsystems.xmldb.model.json.JsonRecord
import at.ac.fhwn.dbsystems.xmldb.repo.JsonRecordRepo
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry


@SpringBootApplication(scanBasePackageClasses = [ProjectPackageClassPath::class])
@EnableJpaRepositories(basePackageClasses = [JsonRecordRepo::class])
@EntityScan(basePackageClasses = [JsonRecord::class])
open class XmlDbSpringBootStarter {
    @Bean
    open fun existDbClient(
        @Value("\${existdb.uri}") uri: String,
        @Value("\${existdb.context}") context: String,
        @Value("\${existdb.user}") user: String,
        @Value("\${existdb.password}") password: String
    ): ExistDbClient {
        val db = ExistDbClient(uri, context, user, password)
        db.registerExistDB()
        return db
    }
}

@Configuration
@EnableWebMvc
open class WebConfig : WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("/gui/**").addResourceLocations("classpath:/static/")
    }

    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addViewController("/").setViewName("redirect:/gui/index.html")
        registry.addViewController("/gui/").setViewName("redirect:/gui/index.html")
    }
}

fun main(args: Array<String>) {
    runApplication<XmlDbSpringBootStarter>(*args)
}
