\select@language {english}
\contentsline {section}{\numberline {1}UseCase}{3}
\contentsline {section}{\numberline {2}Architektur}{3}
\contentsline {section}{\numberline {3}Docker}{4}
\contentsline {section}{\numberline {4}Datenquelle}{6}
\contentsline {section}{\numberline {5}Postgres}{7}
\contentsline {section}{\numberline {6}Erstellung eines Schema}{9}
\contentsline {section}{\numberline {7}XML-Validierung}{14}
\contentsline {subsection}{\numberline {7.1}Implizierte XML-Validierung}{14}
\contentsline {subsection}{\numberline {7.2}Explizite XML-Validierung}{17}
\contentsline {section}{\numberline {8}Backend}{18}
\contentsline {section}{\numberline {9}Exist GUI}{22}
\contentsline {subsection}{\numberline {9.1}Exist Web-GUI}{22}
\contentsline {subsection}{\numberline {9.2}Exist Java Admin Client}{25}
\contentsline {section}{\numberline {10}GUI (React)}{30}
\contentsline {section}{\numberline {11}Jupyter Notebook}{32}
