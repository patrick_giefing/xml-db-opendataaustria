select
  xmlelement(name stations, (
    select xmlagg(
      xmlelement(NAME station,
        xmlelement(NAME geometry_name, json_object ->> 'geometry_name'),
        xmlelement(NAME coordinates,
          xmlelement(NAME long, json_object -> 'geometry' -> 'coordinates' -> 0),
          xmlelement(NAME lat,  json_object -> 'geometry' -> 'coordinates' -> 1)
        ),
        xmlelement(name properties, (select xmlagg(('<' || lower(key) || '>' || value || '</' || lower(key) || '>')::xml) from json_each_text(json_object -> 'properties')))
      )
    )from json_record where record_group = 'metro'
  )
)::text as xml;