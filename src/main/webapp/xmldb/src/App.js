import React, {useState, useEffect} from "react";
import './App.css';
import axios from "axios";
import {getEndoint} from "./rest/rest.environment";
import {Button, ButtonGroup, Dialog, DialogContent, DialogTitle, FormControlLabel, TextField} from "@material-ui/core";
import {Alert} from "@material-ui/lab";
import MetroQueryResults from "./components/MetroQueryResults/MetroQueryResults";
import Map from "./components/Map/GoogleMaps";

function App() {
    const [openYear, setOpenYear] = useState(2013);
    const [exception, setException] = useState(null);
    const [metroQueryResults, setMetroQueryResults] = useState([]);
    const [dialogUrl, setDialogUrl] = useState(null);
    const [infoMessage, setInfoMessage] = useState(null);

    const fetchMetroData = () => {
        axios.get(`${getEndoint()}oda/metro/fetch/`).then(response => {
            setException(null);
            setInfoMessage("Metro data has been loaded into existDB");
            window.setTimeout(() => setInfoMessage(null), 5000);
        }).catch(reason => {
            setException(reason.message);
            setInfoMessage(null);
        });
    };

    const loadMetroDataForOpenYear = (year) => {
        setOpenYear(year);
        axios.get(`${getEndoint()}oda/metro/stations/opened/${year}/`).then(response => {
            setException(null);
            setMetroQueryResults(response.data);
        }).catch(reason => {
            setException(reason.message);
            setMetroQueryResults(null);
        });
    }

    const validateXml = () => {
        setDialogUrl(`${getEndoint()}oda/metro/validate/`);
    };

    const showXml = () => {
        setDialogUrl(`${getEndoint()}oda/metro/xml/`);
    };

    const showXsd = () => {
        setDialogUrl(`${getEndoint()}oda/metro/xsd/`);
    };

    useEffect(() => loadMetroDataForOpenYear(openYear), [openYear])

    let errorMessage = null;
    if (exception) {
        errorMessage = (<Alert severity="error">An error occured: {exception}</Alert>);
    }
    let info = null;
    if (infoMessage) {
        info = (<Alert severity="info">{infoMessage}</Alert>);
    }

    let dialog = null;
    if (dialogUrl) {
        dialog = (<Dialog open={true} onClose={() => setDialogUrl(null)}>
            <DialogTitle>Ressource</DialogTitle>
            <DialogContent>
                <iframe src={dialogUrl} style={{width: "550px", height: "550px"}}></iframe>
            </DialogContent>
        </Dialog>);
    }

    return (
        <div className="App">
            {errorMessage}
            {info}
            {dialog}
            <div>
                <ButtonGroup style={{margin: 10}}>
                    <Button onClick={fetchMetroData}>Read metro data</Button>
                    <Button onClick={validateXml}>Validate xml</Button>
                    <Button onClick={showXml}>Show XML</Button>
                    <Button onClick={showXsd}>Show XSD</Button>
                </ButtonGroup>
                <div style={{margin: "20px"}}>
                    <MetroQueryResults results={metroQueryResults}/>
                </div>
            </div>
            <div>
                <FormControlLabel control={<TextField type="number" value={openYear}
                                                      onChange={e => setOpenYear(+e.target.value)}/>}
                                  label="Eröffnungsjahr:&#160;" labelPlacement="start"/>

            </div>
            <div className='map'>
                <Map stations={metroQueryResults}/>
            </div>
        </div>
    );
}

export default App;
