import React from "react";
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';

export function MapContainer(props) {
    const showMarkers = () => {
        return props.stations.map((station, index) => {
            return (<Marker label={station.station.properties.htxt} key={index} id={index} position={{
                lat: station.station.coordinates.lat,
                lng: station.station.coordinates.long
            }}
                            onClick={() => alert(station.station.properties.htxt)}>
                <span>abc</span>
            </Marker>);
        });
    }

    return (
        <Map google={props.google} style={{height: "calc(100% - 600px)"}} zoom={12} initialCenter={{lat: 48.20849, lng: 16.37208}}>
            {showMarkers()}
        </Map>
    );
}

export default GoogleApiWrapper({
    apiKey: window.googleApiKey,
    language: "de"
})(MapContainer)