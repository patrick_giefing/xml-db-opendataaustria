import React, {useState, useEffect} from 'react';
import PropTypes from "prop-types";
import {DataGrid} from '@material-ui/data-grid';

function MetroQueryResults(props) {
    const columns = [
        {
            field: 'station',
            headerName: 'Station',
            width: 200,
            valueGetter: params => params.row.station.properties.htxt
        },
        {
            field: 'coordinates',
            headerName: 'Coordinates',
            width: 330,
            valueGetter: params => `(${params.row.station.coordinates.long}, ${params.row.station.coordinates.lat})`
        },
        {
            field: 'opening',
            headerName: 'Opening',
            width: 120,
            valueGetter: params => `${params.row.station.properties.eroeffnung_jahr}-${params.row.station.properties.eroeffnung_monat}`
        }
    ];

    return (<div style={{height: 500}}>
        <DataGrid
            rows={props.results}
            columns={columns}
            pageSize={20}
            getRowId={row => row.station.properties.objectid}
        />
    </div>);
}

MetroQueryResults.propTypes = {
    results: PropTypes.array.isRequired
};

export default MetroQueryResults;