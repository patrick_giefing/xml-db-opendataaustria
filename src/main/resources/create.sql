create table json_record
(
    id_record    char(36)    NOT NULL PRIMARY KEY,
    record_group varchar(50) NOT NULL,
    json         text        NOT NULL,
    json_object  json NULL
);

create
or replace function convert_to_json()
RETURNS trigger AS
$$
BEGIN
    --NEW.json_object = json(json)
update json_record
set json_object = json(json)
where json_object is null;
RETURN NEW;
END;
$$
LANGUAGE PLPGSQL;

DROP TRIGGER IF EXISTS tr_json on json_record;

CREATE TRIGGER tr_json
    AFTER INSERT OR
UPDATE OF json
ON json_record
    EXECUTE PROCEDURE convert_to_json();
