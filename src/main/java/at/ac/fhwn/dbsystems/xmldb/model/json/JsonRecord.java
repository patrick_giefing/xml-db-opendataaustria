package at.ac.fhwn.dbsystems.xmldb.model.json;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "json_record")
public class JsonRecord {
    @Id
    @Column(name = "id_record")
    private String recordId;
    @Column(name = "record_group")
    private String recordGroup;
    @Column(name = "json")
    private String json;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getRecordGroup() {
        return recordGroup;
    }

    public void setRecordGroup(String recordGroup) {
        this.recordGroup = recordGroup;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
