package at.ac.fhwn.dbsystems.xmldb;

public class XmlProps {
    public static final String collectionMetro = "/opendataaustria/metro";
    public static final String groupMetro = "metro";
    public static final String xmlFileNameMetro = "metro-stations.xml";
    public static final String xsdFileNameMetro = "metro-stations.xsd";
}
